package puf;

import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.FixedSizeInteger.InvalidSizeException;
import puf.FixedSizeInteger.InvalidValueException;

public class EntanglementPUF extends PUF
{
	private ArbiterKPUF _inputPUF;
	private ArbiterKPUF _outputPUF;
	
	private boolean _unreleasedOnInput = false;
	
	protected EntanglementPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize);

		_inputPUF = new ArbiterKPUF(challengeSize, responseSize);
		_outputPUF = new ArbiterKPUF(challengeSize, responseSize);
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		if (hasBeenReleased()) {
			return generateResponseReleased(challenge, addNoise);
		} else {
			return generateResponseUnreleased(challenge, addNoise);
		}
	}

	@Override
	protected void onRelease()
	{
		// Change the challenge and response size
		try {
			setChallengeSize(2 * _inputPUF.getChallengeSize() + _inputPUF.getResponseSize());
		} catch (InvalidChallengeSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
	}
	
	private Response generateResponseReleased(Challenge challenge, boolean addNoise)
	{
		try {
			Challenge inputChallenge = getInputChallenge(challenge, _inputPUF.getChallengeSize());
			Challenge outputChallenge = getOutputChallenge(challenge, _outputPUF.getChallengeSize());
			
			FixedSizeInteger r1 = getRandomVerificationValue(challenge, _inputPUF.getChallengeSize());
			
			Response inputResponse = _inputPUF.generateResponse(inputChallenge, addNoise);
			Response outputResponse = _outputPUF.generateResponse(outputChallenge, addNoise);
			
			r1.xor(inputResponse);
			r1.xor(outputResponse);
			
			Response ret = new Response(r1.getSize());
			ret.setValue(r1.getBits());
			return ret;
			
		} catch (InvalidSizeException | InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
		
	}
	
	private Response generateResponseUnreleased(Challenge challenge, boolean addNoise)
	{
		_unreleasedOnInput = !_unreleasedOnInput;
		if (_unreleasedOnInput) {
			return _inputPUF.generateResponse(challenge, addNoise);
		} else {
			return _outputPUF.generateResponse(challenge, addNoise);
		}
	}
	
	public static Challenge getInputChallenge(Challenge concatenatedChallenge, int challengeSize)
	{
		try {
			 return concatenatedChallenge.getSubChallenge(0, challengeSize - 1);
		} catch (InvalidBitIndexException e) {
			return null;
		}
	}
	
	public static Challenge getOutputChallenge(Challenge concatenatedChallenge, int challengeSize)
	{
		try {
			 return concatenatedChallenge.getSubChallenge(challengeSize, 2 * challengeSize - 1);
		} catch (InvalidBitIndexException e) {
			return null;
		}
	}
	
	public static FixedSizeInteger getRandomVerificationValue(Challenge concatenatedChallenge, int challengeSize)
	{
		try {
			 return concatenatedChallenge.getSubChallenge(2 * challengeSize, concatenatedChallenge.getSize() - 1);
		} catch (InvalidBitIndexException e) {
			return null;
		}
	}
}
