package puf;

import java.util.HashMap;

import puf.FixedSizeInteger.InvalidSizeException;
import puf.PUF.Challenge;
import puf.PUF.Response;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;

public class Model
{
	private Logistic[] _model = null;
	private Instances[] _trainingSet = null;
	private HashMap<Challenge, Response> _challengesRequiringUpdate = new HashMap<Challenge, Response>();
	private int _responseSize;
	
	public void addTrainingData(Challenge challenge, Response response)
	{
		if (challenge == null || response == null) return;
		
		_responseSize = response.getSize();
		
		_challengesRequiringUpdate.put(challenge, response);
	}
	
	private void update()
	{
		if (_challengesRequiringUpdate.isEmpty()) return;
		
		Enrolment trainingEnrolment = new Enrolment();
		
		for (Challenge seenChallenge : _challengesRequiringUpdate.keySet()) {
			Response trainingResponse = _challengesRequiringUpdate.get(seenChallenge);
			ChallengeResponsePair trainingCRP = ChallengeResponsePair.create(seenChallenge, trainingResponse);
			trainingEnrolment.put(seenChallenge, trainingCRP);
		}
		
		if (_trainingSet == null) {
			_trainingSet = WEKA.createInstancesFromEnrolment("model-training-set", trainingEnrolment);
		} else {
			WEKA.updateInstancesFromEnrolment(_trainingSet, trainingEnrolment);
		}
		
		_model = WEKA.trainModel(_trainingSet);
		
		_challengesRequiringUpdate.clear();
	}
	
	public Response test(Challenge challenge)
	{
		update();
		
		if (_model == null) return null;
		
		// Create a dummy test-set of instances to get WEKA to predict the response
		Response dummyResponse = null;
		try {
			dummyResponse = new Response(_responseSize);
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		ChallengeResponsePair dummyCRP = ChallengeResponsePair.create(challenge, dummyResponse);
		Enrolment dummyEnrolment = new Enrolment();
		dummyEnrolment.put(challenge, dummyCRP);
		Instances[] dummyTestSet = WEKA.createInstancesFromEnrolment("Dummy-test-set", dummyEnrolment);
		HashMap<Response, Response> prediction = WEKA.predictions(_model, _trainingSet, dummyTestSet);
		
		return prediction.get(dummyResponse);
	}
}
