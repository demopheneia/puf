package puf;

import java.util.HashMap;

import puf.FixedSizeInteger.InvalidSizeException;

public class IntegerMapPUF extends PUF
{
	HashMap<Challenge, Response> _map = new HashMap<Challenge, Response>();
	
	public IntegerMapPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize);
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		// addNoise is not used
		
		if (!_map.containsKey(challenge)) {
			try {
				Response response = new Response(getResponseSize());
				response.random();
				_map.put(challenge, response);
			} catch (InvalidSizeException e) {
				Simulation.shouldNeverHappen(e);
				return null;
			}
		}
		
		return _map.get(challenge);
	}

	@Override
	protected void onRelease()
	{
		// Do nothing
	}
}
