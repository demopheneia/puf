package puf;

import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;

public class ArbiterPUFFactory extends PUFFactory
{
	protected int _challengeSize;
	protected int _responseSize;
	
	public ArbiterPUFFactory(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		PUF.InvalidChallengeSizeException.testSettingChallengeSize(challengeSize);
		PUF.InvalidResponseSizeException.testSettingResponseSize(responseSize);
		
		_challengeSize = challengeSize;
		_responseSize = responseSize;
	}
	
	@Override
	protected PUF manufacturePUF()
	{
		try {
			return new ArbiterKPUF(_challengeSize, _responseSize);
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}

	@Override
	protected Server provisionServer()
	{
		return new ArbiterPUFServer();
	}
}
