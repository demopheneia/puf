package puf;

import puf.FixedSizeInteger.InvalidSizeException;

public abstract class PUF
{
	/** The size of challenges to this PUF in bits. */
	private int _challengeSize;
	/** The size of responses from this PUF in bits. */
	private int _responseSize;
	
	/** Whether this PUF has been released from the factory yet. */
	private boolean _released = false;
	
	/** Constructor must set valid challenge/response sizes on instantiation. */
	protected PUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		setChallengeSize(challengeSize);
		setResponseSize(responseSize);
	}
	
	/** Generates a challenge-response pair for the given challenge. */
	public final ChallengeResponsePair challenge(Challenge challenge, boolean addNoise) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		InvalidChallengeSizeException.testApplyingChallenge(challenge, this);
		
		Response response = generateResponse(challenge, addNoise);
		
		InvalidResponseSizeException.testGeneratedResponse(response, this);
		
		return ChallengeResponsePair.create(challenge, response);
	}
	
	/** Actual response-generation algorithm to be implemented by PUF. */
	protected abstract Response generateResponse(Challenge challenge, boolean addNoise);
	
	/** Gets the size of a challenge to this PUF (in bits). */
	public final int getChallengeSize()
	{
		return _challengeSize;
	}

	/** Gets the size of a response from this PUF (in bits). */
	public final int getResponseSize()
	{
		return _responseSize;
	}
	
	/** Sets the challenge-size for this PUF (in bits). */
	protected final void setChallengeSize(int size) throws InvalidChallengeSizeException
	{
		InvalidChallengeSizeException.testSettingChallengeSize(size);
		
		_challengeSize = size;
	}
	
	/** Sets the response-size for this PUF (in bits). */
	protected final void setResponseSize(int size) throws InvalidResponseSizeException
	{
		InvalidResponseSizeException.testSettingResponseSize(size);
		
		_responseSize = size;
	}
	
	/** Creates an uninitialised response of the correct size for this PUF. */
	protected final Response getCorrectlySizedResponse()
	{
		try {
			return new Response(getResponseSize());
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	/** Whether this PUF has been released yet. */
	public boolean hasBeenReleased()
	{
		return _released;
	}
	
	/** Releases the PUF into the wild. */
	public void release()
	{
		onRelease();
		_released = true;
	}
	
	/** Allows the sub-class to specify modifications made on release. */
	protected abstract void onRelease();
	
	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " :\n" +
				"  #C = " + getChallengeSize() + " bits\n" +
				"  #R = " + getResponseSize() + " bits";
	}
	
	public static class Challenge extends FixedSizeInteger
	{
		public Challenge(int size) throws InvalidSizeException
		{
			super(size);
		}
		
		public Challenge(Challenge other) throws InvalidValueException
		{
			super(other);
		}
		
		public Challenge getSubChallenge(int lowerBitIndex, int upperBitIndex) throws InvalidBitIndexException
		{
			FixedSizeInteger extractedBits = this.getBits(lowerBitIndex, upperBitIndex);
			
			try {
				Challenge subChallenge = new Challenge(extractedBits.getSize());
				subChallenge.setValue(extractedBits.getBits());
				return subChallenge;
			} catch (InvalidSizeException | InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
				return null;
			}
		}
		
		public Challenge clone()
		{
			try {
				return new Challenge(this);
			} catch (InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
				return null;
			}
		}
	}
	
	public static class Response extends FixedSizeInteger
	{
		public Response(int size) throws InvalidSizeException
		{
			super(size);
		}
		
		public Response(Response other) throws InvalidValueException
		{
			super(other);
		}
		
		public static Response concatenate(Response one, Response two)
		{
			FixedSizeInteger concatenation = FixedSizeInteger.concatenate(one, two);
			try {
				Response ret = new Response(concatenation.getSize());
				ret.setValue(concatenation.getBits());
				return ret;
			} catch (InvalidSizeException | InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
				return null;
			}
			
		}
		
		public Response clone()
		{
			try {
				return new Response(this);
			} catch (InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
				return null;
			}
		}
	}
	
	public static class InvalidChallengeSizeException extends Exception
	{
		private static final long serialVersionUID = 6383458474206403996L;
		
		public static final void testSettingChallengeSize(int challengeSize) throws InvalidChallengeSizeException
		{
			if (challengeSize <= 0) throw new InvalidChallengeSizeException("Challenge size cannot be negative or zero");
		}
		
		public static final void testApplyingChallenge(Challenge challenge, PUF target) throws InvalidChallengeSizeException
		{
			if (target == null) return;
			
			if (challenge == null || challenge.getSize() != target._challengeSize) throw new InvalidChallengeSizeException("Applied challenge size (" + challenge.getSize() + ") does not match target PUF (challenge-size = " + target._challengeSize + ")");
		}
		
		private InvalidChallengeSizeException(String message)
		{
			super(message);
		}
	}
	
	public static class InvalidResponseSizeException extends Exception
	{
		private static final long serialVersionUID = 6383458474206403996L;
		
		public static final void testSettingResponseSize(int responseSize) throws InvalidResponseSizeException
		{
			if (responseSize <= 0) throw new InvalidResponseSizeException("Challenge size cannot be negative or zero");
		}
		
		public static final void testGeneratedResponse(Response response, PUF source) throws InvalidResponseSizeException
		{
			if (source == null || response == null) return;
			
			if (response.getSize() != source._responseSize) throw new InvalidResponseSizeException("Generated response size (" + response.getSize() + ") does not match target PUF (response-size = " + source._responseSize + ")");
		}
		
		private InvalidResponseSizeException(String message)
		{
			super(message);
		}
	}
}
