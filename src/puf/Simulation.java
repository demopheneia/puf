package puf;

import java.io.FileOutputStream;
import java.io.PrintWriter;

import puf.FixedSizeInteger.InvalidSizeException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public class Simulation
{
	public static final int TEST_BATCH_SIZE = 1000;
	public static final int CHALLENGE_SIZE = 32;
	public static final int RESPONSE_SIZE = 16;
	public static final int XOR_SIZE = 2;
	public static final boolean ATTACKER_EXISTS = true;
	public static final boolean ATTACKER_HAS_DIRECT_ACCESS = false;
	public static final boolean ATTACKER_SEES_SERVER_VERIFICATION_EVENTS = !ATTACKER_HAS_DIRECT_ACCESS;
	
	/** The current PUF-type under test (factory). */
	public static PUFFactory FACTORY = null;
	static {
		try {
			FACTORY = new AESKPUFFactory(CHALLENGE_SIZE, RESPONSE_SIZE);
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			shouldNeverHappen(e);
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		// Instantiate the PUF and it's corresponding server
		Pair<PUF, Server> pufAndServer = FACTORY.purchase();
		PUF puf = pufAndServer.first();
		Server server = pufAndServer.second();

		System.out.println(puf);
		System.out.println(server);
		
		Attacker attacker = null;
		if (ATTACKER_EXISTS) {
			attacker = new ArbiterPUFAttacker(puf.getChallengeSize(), puf.getResponseSize(), server);
			System.out.println(attacker);
			if (ATTACKER_HAS_DIRECT_ACCESS) attacker.getDirectAccessToPUF(puf);
		}
		
		PrintWriter output = new PrintWriter(new FileOutputStream("/home/cts16/output.txt"));
		
		System.out.println("");
		
		int n = 0;
		while (!server.isExhausted()) {
			int truePositives = 0, falsePositives = 0, trueNegatives = 0, falseNegatives = 0;
			n += TEST_BATCH_SIZE;
			
			for (int i = 0; i < TEST_BATCH_SIZE; i++) {
				if (ATTACKER_SEES_SERVER_VERIFICATION_EVENTS && server.verify(puf, attacker)) truePositives++;
				else if (!ATTACKER_SEES_SERVER_VERIFICATION_EVENTS && server.verify(puf, null)) truePositives++; 
				else falseNegatives++;
			}
			
			if (ATTACKER_EXISTS) {
				for (int i = 0; i < TEST_BATCH_SIZE; i++) {
					if (server.verify(attacker.getDummyPUF(), null)) falsePositives++;
					else trueNegatives++;
				}
			}
			
			String line = n + ": T = " + Util.percent(truePositives, truePositives + falseNegatives, 1);
			if (ATTACKER_EXISTS) line += ", F = " + Util.percent(falsePositives, falsePositives + trueNegatives, 1);
			output.println(line);
			System.out.println(line);
		}
		
		output.flush();
		output.close();
	}
	
	/** Prints the entire enrolment map of CRPs. */
	public static String formatEnrolment(Enrolment enrolment)
	{
		if (enrolment == null) return null;
		
		String ret = null;
		
		for (ChallengeResponsePair crp : enrolment.values()) {
			if (ret == null) {
				ret = crp.toString(false);
			} else {
				ret += "\n" + crp.toString(false);
			}
		}
		
		return ret;
	}
	
	/** Tries to calculate the error-rate of a noisy PUF. */
	public static double calculateErrorRate(PUF puf)
	{
		try {
			Challenge c = new Challenge(puf.getChallengeSize());
			
			Response canonical = puf.challenge(c, false).getResponse();
			
			double errorRate = 0.0;
			for (int i = 0; i < 10000; i++) {
				Response test = puf.challenge(c, true).getResponse();
				System.out.println(test);
				if (test.hammingDistanceTo(canonical) != 0) errorRate += 0.0001;
			}
			
			return errorRate;
		} catch (InvalidSizeException | InvalidResponseSizeException | InvalidChallengeSizeException e) {
			shouldNeverHappen(e);
			return 0.0;
		}
	}
	
	/** Called for exceptions that shouldn't occur. */
	public static void shouldNeverHappen(Exception e)
	{
		System.err.println("An exception, that should not have occurred, occurred");
		System.err.println(e.toString());
		e.printStackTrace();
		System.exit(-1);
	}
}
