package puf;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import puf.FixedSizeInteger.InvalidSizeException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public class ArbiterPUFServer extends Server
{
	private boolean _exhausted = false;
	
	public ArbiterPUFServer()
	{
		
	}
	
	@Override
	public Set<Challenge> createEnrolmentChallenges(PUF puf)
	{
		return createEnrolmentChallengesPublic(puf, NUMBER_OF_CHALLENGES_TO_ENROL);
	}
	
	public static Set<Challenge> createEnrolmentChallengesPublic(PUF puf, int amount)
	{
		Set<Challenge> challenges = new HashSet<Challenge>(amount);
		
		for (int i = 0; i < amount; i++) {
			try {
				Challenge challenge = new Challenge(puf.getChallengeSize());
				challenge.random();
				if (challenges.contains(challenge)) {
					i--;
					continue;
				}
				challenges.add(challenge);
			} catch (InvalidSizeException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		return challenges;
	}

	@Override
	public boolean verify(PUF puf, Attacker watcher)
	{
		if (_exhausted) return false;
		
		Set<Challenge> availableChallenges = getEnrolledChallenges();
		Iterator<Challenge> it = availableChallenges.iterator();
		
		if (it.hasNext() == false) {
			_exhausted = true;
			return false;
		}
		
		Challenge verificationChallenge = it.next();
		Response expectedResponse = getEnrolledResponse(verificationChallenge);
		
		Response actualResponse = null;
		try {
			ChallengeResponsePair crp = puf.challenge(verificationChallenge, true);
			if (crp == null) return false;
			actualResponse = crp.getResponse();
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			return false;
		}
		
		// The attacker sees the challenge and response
		if (watcher != null) watcher.seeVerificationEvent(verificationChallenge, actualResponse);
		
		// Can't reuse the challenge again now that the attacker has seen it
		removeEnrolledChallenge(verificationChallenge);
		
		int hammingDistance = Integer.MAX_VALUE;
		try {
			hammingDistance = expectedResponse.hammingDistanceTo(actualResponse);
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		return hammingDistance <= getMaxAllowableHammingError();
	}
	
	@Override
	public boolean isExhausted()
	{
		return _exhausted;
	}
	
	@Override
	protected double calculateFalseNegativeProbability(int responseSize, int allowableFlippedBits)
	{
		double probability = 0.0;
		double pBitFlipped = ArbiterPUF.PROBABILITY_OF_BIT_FLIP;
		double pBitUnflipped = 1.0 - pBitFlipped;
		for (int i = 0; i <= allowableFlippedBits; i++) {
			double branchCount = Util.choose(responseSize, i).doubleValue();
			double productPFlipped = Math.pow(pBitFlipped, i);
			double productPUnflipped = Math.pow(pBitUnflipped, responseSize - i);
			probability += branchCount * productPFlipped * productPUnflipped;
		}
		return 1.0 - probability;
	}
}
