package puf;

import puf.ArbiterKXORPUF.InvalidXORSizeException;

public class ArbiterXORPUFServer extends ArbiterPUFServer
{
	private int _xorSize;
	
	public ArbiterXORPUFServer(int xorSize) throws InvalidXORSizeException
	{
		ArbiterKXORPUF.InvalidXORSizeException.testSettingXORSize(xorSize);
		
		_xorSize = xorSize;
	}
	
	@Override
	protected double calculateFalseNegativeProbability(int responseSize, int allowableFlippedBits)
	{	
		double probability = 0.0;
		double pBitFlipped = ArbiterPUF.PROBABILITY_OF_BIT_FLIP;
		double pBitUnflipped = 1.0 - pBitFlipped;
		
		double pXorFlipped = 0.0;
		double pXorUnflipped = 0.0;
		
		for (int i = 1; i <= _xorSize; i += 2) {
			double branchCount = Util.choose(_xorSize, i).doubleValue();
			double productPFlipped = Math.pow(pBitFlipped, i);
			double productPUnflipped = Math.pow(pBitUnflipped, _xorSize - i);
			pXorFlipped += branchCount * productPFlipped * productPUnflipped;
		}
		
		pXorUnflipped = 1.0 - pXorFlipped;
		
		for (int i = 0; i <= allowableFlippedBits; i++) {
			double branchCount = Util.choose(responseSize, i).doubleValue();
			double productPFlipped = Math.pow(pXorFlipped, i);
			double productPUnflipped = Math.pow(pXorUnflipped, responseSize - i);
			probability += branchCount * productPFlipped * productPUnflipped;
		}
		
		return 1.0 - probability;
	}
}
