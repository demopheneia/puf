package puf;

import puf.FixedSizeInteger.InvalidSizeException;

public class ArbiterKXORPUF extends PUF
{
	private ArbiterKPUF[] _kPUFs;
	private int _nXORStages;
	
	protected ArbiterKXORPUF(int challengeSize, int responseSize, int nXORStages) throws InvalidChallengeSizeException, InvalidResponseSizeException, InvalidXORSizeException
	{
		super(challengeSize, responseSize);
		
		InvalidXORSizeException.testSettingXORSize(nXORStages);

		_kPUFs = new ArbiterKPUF[nXORStages];
		_nXORStages = nXORStages;
		
		for (int i = 0; i < nXORStages; i++) {
			_kPUFs[i] = new ArbiterKPUF(challengeSize, responseSize);
		}
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		Response response = getCorrectlySizedResponse();
		
		for (int i = 0; i < _nXORStages; i++) {
			try {
				response.xor(_kPUFs[i].generateResponse(challenge, addNoise));
			} catch (InvalidSizeException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		return response;
	}
	
	public Response[] generateUnXORedResponses(Challenge challenge, boolean addNoise)
	{
		// Unavailable after release
		if (this.hasBeenReleased()) return null;
		
		Response[] responses = new Response[_nXORStages];
		
		for (int i = 0; i < _nXORStages; i++) {
			responses[i] = _kPUFs[i].generateResponse(challenge, addNoise);
		}
		
		return responses;
	}

	@Override
	protected void onRelease()
	{
		// Do nothing
	}
	
	@Override
	public String toString()
	{
		return super.toString() + "\n" +
				"  #X = " + _nXORStages;
	}
	
	public static class InvalidXORSizeException extends Exception
	{
		private static final long serialVersionUID = -7119638232050879418L;

		public static final void testSettingXORSize(int xorSize) throws InvalidXORSizeException
		{
			if (xorSize < 2) throw new InvalidXORSizeException("XOR size cannot be less than two.");
		}
		
		private InvalidXORSizeException(String message)
		{
			super(message);
		}
	}

}
