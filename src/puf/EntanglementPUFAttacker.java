package puf;

import java.util.HashMap;
import java.util.Map;

import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.FixedSizeInteger.InvalidSizeException;
import puf.FixedSizeInteger.InvalidValueException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public class EntanglementPUFAttacker extends Attacker
{
	private Map<Challenge, Response> _seenEvents = new HashMap<Challenge, Response>();
	private int _challengeSize = -1;
	private int _expandedChallengeSize = 0;
	
	private Model _model;
	
	public EntanglementPUFAttacker(int challengeSize, int responseSize, Server server) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize, server);
		
		_challengeSize = (challengeSize - responseSize) / 2;
		
		_model = new Model();
	}

	@Override
	public void seeVerificationEvent(Challenge challenge, Response response)
	{
		if (challenge == null || response == null) return;
		
		// Does nothing if the dummy PUF is already instantiated
		try {
			instantiateDummyPUF(challenge.getSize(), response.getSize());
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		Challenge inputChallenge = EntanglementPUF.getInputChallenge(challenge, _challengeSize);
		Challenge outputChallenge = EntanglementPUF.getOutputChallenge(challenge, _challengeSize);
		FixedSizeInteger randomVerificationValue = EntanglementPUF.getRandomVerificationValue(challenge, _challengeSize);
		
		try {
			response.xor(randomVerificationValue);
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		Challenge concatenatedChallenge = createChallengeKey(inputChallenge, outputChallenge);
		_seenEvents.put(concatenatedChallenge, response);
		
		_model.addTrainingData(formatChallenge(inputChallenge, outputChallenge), response);
	}

	@Override
	public void getDirectAccessToPUF(PUF puf)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Response guessResponse(Challenge challenge)
	{
		if (_challengeSize == -1) return null;
		
		Challenge inputChallenge = EntanglementPUF.getInputChallenge(challenge, _challengeSize);
		Challenge outputChallenge = EntanglementPUF.getOutputChallenge(challenge, _challengeSize);
		FixedSizeInteger randomVerificationValue = EntanglementPUF.getRandomVerificationValue(challenge, _challengeSize);

		Challenge concatenatedChallenge = createChallengeKey(inputChallenge, outputChallenge);
		
		if (_seenEvents.containsKey(concatenatedChallenge)) {
			try {
				randomVerificationValue.xor(_seenEvents.get(concatenatedChallenge));
				Response ret = new Response(randomVerificationValue.getSize());
				ret.setValue(randomVerificationValue.getBits());
				return ret;
			} catch (InvalidSizeException | InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		Response xor = _model.test(formatChallenge(inputChallenge, outputChallenge));
		
		if (xor == null) return null;
		
		try {
			xor.xor(randomVerificationValue);
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
		
		return xor;
	}

	@Override
	public int getExpandedChallengeSize()
	{
		if (_expandedChallengeSize == 0) _expandedChallengeSize = _challengeSize * _challengeSize;
		
		return _expandedChallengeSize;
	}
	
	private Challenge formatChallenge(Challenge inputChallenge, Challenge outputChallenge)
	{
		if (inputChallenge == null || outputChallenge == null) return null;
		
		Challenge pic = ArbiterPUFAttacker.phiIfyChallenge(inputChallenge);
		Challenge poc = ArbiterPUFAttacker.phiIfyChallenge(outputChallenge);
		
		return squareChallenge(pic, poc);
	}
	
	protected Challenge squareChallenge(Challenge ic, Challenge oc)
	{
		if (ic == null || oc == null) return null;
		
		Challenge ret = createExpandedChallenge();
		
		int retIndex = 0;
		for (int inputIndex = 0; inputIndex < ic.getSize(); inputIndex++) {
			for (int outputIndex = 0; outputIndex < oc.getSize(); outputIndex++) {
				try {
					ret.xorBit(retIndex, ic.getBit(inputIndex) ^ oc.getBit(outputIndex));
				} catch (InvalidBitIndexException e) {
					Simulation.shouldNeverHappen(e);
				}
				retIndex++;
			}
		}
		
		return ret;
	}
	
	private Challenge createChallengeKey(Challenge inputChallenge, Challenge outputChallenge)
	{
		try {
			Challenge concatenatedChallenge = new Challenge(inputChallenge.getSize() + outputChallenge.getSize());
			concatenatedChallenge.setValue(FixedSizeInteger.concatenate(inputChallenge, outputChallenge).getBits());
			return concatenatedChallenge;
		} catch (InvalidSizeException | InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}

}
