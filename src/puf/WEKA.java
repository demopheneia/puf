package puf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.PUF.Response;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

public class WEKA
{
	public static HashMap<Response, Response> predictions(Logistic[] model, Instances[] trainingSet, Instances[] testSet)
	{
		if (model == null || testSet == null || trainingSet == null) return null;
		
		HashMap<Response, Response> ret = new HashMap<Response, Response>();
		
		try {
			Evaluation[] evaluations = new Evaluation[model.length];
			
			int responseBitIndex = testSet[0].get(0).classIndex();
			
			for (int modelComponentIndex = 0; modelComponentIndex < model.length; modelComponentIndex++) {
				evaluations[modelComponentIndex] = new Evaluation(trainingSet[modelComponentIndex]);
			}
			
			for (int instanceIndex = 0; instanceIndex < testSet[0].size(); instanceIndex++) {
				Response correctResponse = new Response(model.length);
				Response predictedResponse = new Response(model.length);
				for (int modelComponentIndex = 0; modelComponentIndex < model.length; modelComponentIndex++) {
					double predictedBit = evaluations[modelComponentIndex].evaluateModelOnce(model[modelComponentIndex], testSet[modelComponentIndex].instance(instanceIndex));
					if (testSet[modelComponentIndex].get(instanceIndex).value(responseBitIndex) == 1.0) correctResponse.setBit(modelComponentIndex);
					if (predictedBit == 1.0) predictedResponse.setBit(modelComponentIndex);
				}
				ret.put(correctResponse, predictedResponse);
			}
		} catch (Exception e) {
			System.err.println("Error evaluating model!!!");
			return null;
		}
		
		return ret;
	}
	
	/** Writes an ARFF file containing the given instances. */
	public static void writeInstancesToARFFFile(Instances instances, String filename) throws FileNotFoundException, IOException
	{
		if (instances == null || filename == null) return;
		
		ArffSaver saver = new ArffSaver();
		FileOutputStream output = new FileOutputStream(filename);
		saver.setDestination(output);
		saver.setInstances(instances);
		saver.writeBatch();
		output.flush();
		output.close();
	}
	
	/** Uses WEKA to train a model of the data. */
	public static Logistic[] trainModel(Instances[] data)
	{
		String[] options = new String[6];
		options[0] = "-R";
		options[1] = "0.0";
		options[2] = "-M";
		options[3] = "-1";
		options[4] = "-num-decimal-places";
		options[5] = "4";
		
		Logistic[] ret = new Logistic[data.length];
		
		for (int i = 0; i < data.length; i++) {
			try {
				ret[i] = trainModel(data[i], options);
			} catch (Exception e) {
				System.err.println("Exception training model");
				System.err.println(e.getMessage());
				return null;
			}
		}
		
		return ret;
	}
	
	/** Uses WEKA to train a model of the data. 
	 * @throws Exception */
	public static Logistic trainModel(Instances data, String[] options) throws Exception
	{
		Logistic model = new Logistic();
		
		model.setOptions(options);
		model.buildClassifier(data);
		
		return model;
	}
	
	/** Takes an enrolment of CRPs and converts them to the format required for WEKA. */
	public static Instances[] createInstancesFromEnrolment(String name, Enrolment enrolment)
	{
		if (enrolment == null || enrolment.size() == 0 || name == null) return null;
		
		ArrayList<String> labels = new ArrayList<String>();
		labels.add("0");
		labels.add("1");
		
		ArrayList<Attribute> challengeAttributes = new ArrayList<Attribute>();
		int numChallengeAttributes = enrolment.keySet().iterator().next().getSize();
		for (int i = 0; i < numChallengeAttributes; i++) {
			//challengeAttributes.add(new Attribute("challenge-bit-" + i, labels));
			challengeAttributes.add(new Attribute("challenge-bit-" + i));
		}

		ArrayList<Attribute> responseAttributes = new ArrayList<Attribute>();
		int numResponseAttributes = enrolment.values().iterator().next().getResponse().getSize();
		for (int i = 0; i < numResponseAttributes; i++) {
			responseAttributes.add(new Attribute("response-bit-" + i, labels));
		}
		
		Instances[] instancesArray = new Instances[numResponseAttributes];
		
		for (int i = 0; i < numResponseAttributes; i++) {
			ArrayList<Attribute> attributes = new ArrayList<Attribute>(challengeAttributes);
			attributes.add(responseAttributes.get(i));
			
			Instances instances = new Instances(name + "-" + i, attributes, enrolment.size());
			instances.setClassIndex(attributes.size() - 1);
			
			Iterator<ChallengeResponsePair> it = enrolment.values().iterator();
			while (it.hasNext()) {
				try {
					instances.add(new DenseInstance(1.0, crpToDoubleArray(it.next(), i)));
				} catch (InvalidBitIndexException e) {
					Simulation.shouldNeverHappen(e);
				}
			}
			
			instancesArray[i] = instances;
		}
		
		return instancesArray;
	}

	public static void updateInstancesFromEnrolment(Instances[] instancesArray, Enrolment enrolment)
	{
		if (enrolment == null || enrolment.size() == 0) return;
		
		int numResponseAttributes = enrolment.values().iterator().next().getResponse().getSize();
		
		for (int i = 0; i < numResponseAttributes; i++) {
			Instances instances = instancesArray[i];
			
			Iterator<ChallengeResponsePair> it = enrolment.values().iterator();
			while (it.hasNext()) {
				try {
					instances.add(new DenseInstance(1.0, crpToDoubleArray(it.next(), i)));
				} catch (InvalidBitIndexException e) {
					Simulation.shouldNeverHappen(e);
				}
			}
		}
	}
	
	public static double[] crpToDoubleArray(ChallengeResponsePair crp, int responseBitIndex) throws InvalidBitIndexException
	{
		return crpToDoubleArray(crp, null, responseBitIndex);
	}
	
	public static double[] crpToDoubleArray(ChallengeResponsePair crp, double[] prealloc, int responseBitIndex) throws InvalidBitIndexException
	{
		int arraySize = crp.getChallenge().getSize() + 1;
		
		if (prealloc == null || prealloc.length < arraySize) {
			prealloc = new double[arraySize];
		}
		
		fixedSizeIntegerToDoubleArray(crp.getChallenge(), prealloc);

		prealloc[arraySize - 1] = booleanToDouble(crp.getResponse().getBit(responseBitIndex));
		
		return prealloc;
	}

	public static double[] fixedSizeIntegerToDoubleArray(FixedSizeInteger n)
	{
		return fixedSizeIntegerToDoubleArray(n, null);
	}
	
	public static double[] fixedSizeIntegerToDoubleArray(FixedSizeInteger n, double[] prealloc)
	{
		if (prealloc == null || prealloc.length < n.getSize()) {
			prealloc = new double[n.getSize()];
		}
		
		for (int i = 0; i < n.getSize(); i++) {
			try {
				prealloc[i] = booleanToDouble(n.getBit(i));
			} catch (InvalidBitIndexException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		return prealloc;
	}
	
	public static double booleanToDouble(boolean b)
	{
		return b ? 1.0 : 0.0;
	}
	
	public static double scale(double in)
	{
		return 2 * in - 1;
	}
}
