package puf;

import java.security.SecureRandom;

public class PUFRandom extends SecureRandom
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6926798464574294298L;

	private static PUFRandom _instance;
	
	public static PUFRandom getInstance()
	{
		if (_instance == null) _instance = new PUFRandom();
		
		return _instance;
	}
	
	public static double getDouble(double min, double max)
	{
		return getInstance().nextDouble() * (max - min) + min;
	}
	
	public static int getInt(int min, int max)
	{
		return (int) getDouble(min, max);
	}
	
	public static double getGaussian(double mean, double stddev)
	{
		return getInstance().nextGaussian() * stddev + mean;
	}
	
	private PUFRandom()
	{
	}
}
