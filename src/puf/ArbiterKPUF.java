package puf;

import puf.FixedSizeInteger.InvalidBitIndexException;

public class ArbiterKPUF extends PUF
{
	private ArbiterPUF[] _arbiterPUFs;

	protected ArbiterKPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize);

		_arbiterPUFs = new ArbiterPUF[responseSize];
		
		for (int i = 0; i < responseSize; i++) {
			_arbiterPUFs[i] = new ArbiterPUF(challengeSize);
		}
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		Response response = getCorrectlySizedResponse();
		
		for (int i = 0; i < getResponseSize(); i++) {
			try {
				if (_arbiterPUFs[i].generateResponse(challenge, addNoise).getBit(0)) response.setBit(i);
			} catch (InvalidBitIndexException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		return response;
	}

	@Override
	protected void onRelease()
	{
		// Do nothing
	}

}
