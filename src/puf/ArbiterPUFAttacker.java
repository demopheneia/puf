package puf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.FixedSizeInteger.InvalidSizeException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public class ArbiterPUFAttacker extends Attacker
{
	/** Number of samples to take to try and reduce noise in the response. Odd to prevent ties. */
	public static final int NOISE_REDUCTION_SAMPLES = 21;
	
	private HashMap<Challenge, Response> _seenCRPs = new HashMap<Challenge, Response>();
	
	private Model _model;
	
	public ArbiterPUFAttacker(int challengeSize, int responseSize, Server server) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize, server);
		
		_model = new Model();
	}
	
	@Override
	public void seeVerificationEvent(Challenge challenge, Response response)
	{
		if (challenge == null || response == null) return;
		
		// Does nothing if the dummy PUF is already instantiated
		try {
			instantiateDummyPUF(challenge.getSize(), response.getSize());
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		_seenCRPs.put(challenge, response);
		_model.addTrainingData(formatChallenge(challenge), response);
	}

	@Override
	public void getDirectAccessToPUF(PUF puf)
	{
		if (puf == null) return;
		
		// Does nothing if the dummy PUF is already instantiated
		try {
			instantiateDummyPUF(puf.getChallengeSize(), puf.getResponseSize());
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		// Calculate the number of CRPs to enrol
		int nCRPs = estimateDesiredNumberOfCRPs();
		
		for (Challenge challenge : ArbiterPUFServer.createEnrolmentChallengesPublic(puf, nCRPs)) {
			ArrayList<Response> samples = new ArrayList<Response>(NOISE_REDUCTION_SAMPLES);
			for (int sampleIndex = 0; sampleIndex < NOISE_REDUCTION_SAMPLES; sampleIndex++) {
				Response response = null;
				try {
					response = puf.challenge(challenge, true).getResponse();
				} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
					Simulation.shouldNeverHappen(e);
				}
				
				samples.add(response);
			}

			// Rest of handling is the same as seeing a legitimate verification event
			seeVerificationEvent(challenge, noiseReducedResponse(samples));
		}
	}

	@Override
	public Response guessResponse(Challenge challenge)
	{
		if (challenge == null || getDummyPUF() == null) return null;
		
		if (_seenCRPs.containsKey(challenge)) {
			return _seenCRPs.get(challenge);
		}
		
		return _model.test(formatChallenge(challenge));
	}
	
	protected Challenge formatChallenge(Challenge challenge)
	{
		return phiIfyChallenge(challenge);
	}
	
	public static final Challenge phiIfyChallenge(Challenge challenge)
	{
		if (challenge == null) return null;
		
		try {
			Challenge ret = new Challenge(challenge.getSize());
			
			if (challenge.getBit(0)) ret.setBit(0);
			
			for (int i = 1; i < challenge.getSize(); i++) {
				if (challenge.getBit(i)) {
					if (!ret.getBit(i - 1)) ret.setBit(i);
				} else {
					if (ret.getBit(i - 1)) ret.setBit(i);
				}
			}
			
			return ret;
		} catch (InvalidSizeException | InvalidBitIndexException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	protected final Response noiseReducedResponse(Collection<Response> samples)
	{
		Response ret = null;
		try {
			ret = getDummyPUF().getCorrectlySizedResponse();
			ret.xor(FixedSizeInteger.noiseReducedValue(samples));
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
		return ret;
	}

	@Override
	public int getExpandedChallengeSize()
	{
		return this.getDummyPUF().getChallengeSize();
	}
}
