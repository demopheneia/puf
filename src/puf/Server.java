package puf;

import java.util.Set;

import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public abstract class Server
{
	public static final int NUMBER_OF_CHALLENGES_TO_ENROL = 100000;
	public static final double DESIRED_FALSE_NEGATIVE_PROBABILITY = 0.1;
	
	protected Enrolment _enrolment = null;
	
	private int _maxAllowableHammingError = -1;
	private double _achievedFalseNegativeProbability = 1.0;
	
	public Server()
	{
	}
	
	public void enrol(PUF puf)
	{
		// Can only enrol once
		if (_enrolment != null) return;
		
		// Create the enrolment challenge set
		Set<Challenge> challenges = createEnrolmentChallenges(puf);
		
		// Initialise the enrolment
		_enrolment = new Enrolment(challenges.size());
		
		// Create the required number of CRPs
		for (Challenge challenge : challenges) {
			
			// Enrol the CRP
			ChallengeResponsePair crp;
			try {
				crp = puf.challenge(challenge, false);
				_enrolment.put(challenge, crp);
			} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		// Calculate the allowed Hamming error for verification
		setMaxAllowableHammingError(puf);
	}
	
	protected final Response getEnrolledResponse(Challenge challenge)
	{
		return getEnrolledResponse(challenge, _enrolment);
	}
	
	protected final Response getEnrolledResponse(Challenge challenge, Enrolment enrolment)
	{
		if (enrolment == null) return null;
		
		return enrolment.getResponse(challenge);
	}
	
	protected final Set<Challenge> getEnrolledChallenges()
	{
		return getEnrolledChallenges(_enrolment);
	}
	
	protected final Set<Challenge> getEnrolledChallenges(Enrolment enrolment)
	{
		if (enrolment == null) return null;
		
		return enrolment.getChallenges();
	}
	
	protected final Challenge getRandomEnrolledChallenge()
	{
		return getRandomEnrolledChallenge(_enrolment);
	}
	
	protected final Challenge getRandomEnrolledChallenge(Enrolment enrolment)
	{
		if (enrolment == null) return null;
		
		return enrolment.getRandomChallenge();
	}
	
	protected void removeEnrolledChallenge(Challenge challenge)
	{
		_enrolment.remove(challenge);
	}
	
	public void setMaxAllowableHammingError(PUF puf)
	{
		Pair<Integer, Double> pair = calculateMaxAllowableHammingError(puf.getResponseSize());
		_maxAllowableHammingError = pair.first();
		_achievedFalseNegativeProbability = pair.second();
	}
	
	public int getMaxAllowableHammingError()
	{
		return _maxAllowableHammingError;
	}
	
	public double getAchievedFalseNegativeProbability()
	{
		return _achievedFalseNegativeProbability;
	}

	private Pair<Integer, Double> calculateMaxAllowableHammingError(int responseSize)
	{
		for (int i = 0; i < responseSize; i++) {
			double pFalseNegative = calculateFalseNegativeProbability(responseSize, i);
			if (pFalseNegative < DESIRED_FALSE_NEGATIVE_PROBABILITY) {
				return new Pair<Integer, Double>(i, pFalseNegative);
			}
		}
		
		return new Pair<Integer, Double>(responseSize, 0.0);
	}
	
	public abstract Set<Challenge> createEnrolmentChallenges(PUF puf);
	
	public abstract boolean verify(PUF puf, Attacker watcher);
	
	protected abstract double calculateFalseNegativeProbability(int responseSize, int allowableFlippedBits);
	
	public abstract boolean isExhausted();
	
	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " :\n" +
				"  pFN = " + Util.percent(getAchievedFalseNegativeProbability(), 1.0, 3) + " (" + Util.percent(DESIRED_FALSE_NEGATIVE_PROBABILITY, 1.0, 1) + ")\n" +
				"  mHE = " + getMaxAllowableHammingError();
	}
}
