package puf;

import java.math.BigInteger;

import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.FixedSizeInteger.InvalidValueException;

public class ArbiterPUF extends PUF
{
	private static final double MEAN_DELAY = 1.0;
	private static final double STDDEV_DELAY = 0.1;
	
	public static final double PROBABILITY_OF_BIT_FLIP = 0.0482;
	
	private double[] _sigma_0, _sigma_1;
	
	protected ArbiterPUF(int challengeSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, 1);
		
		generateSigma();
	}
	
	public String toString()
	{
		String output = "{\n\t";
		
		for (int i = 0; i < this.getChallengeSize(); i++) {
			output += i + ":" + _sigma_0[i] + " ";
		}
		
		output += "\n\t";
		
		for (int i = 0; i < this.getChallengeSize(); i++) {
			output += i + ":" + _sigma_1[i] + " ";
		}
		
		output += "\n}";
		
		return output;	
	}
	
	private void generateSigma()
	{
		int sigmaSize = this.getChallengeSize();
		
		_sigma_0 = new double[sigmaSize];
		_sigma_1 = new double[sigmaSize];
		
		for (int i = 0; i < sigmaSize; i++) {
			_sigma_0[i] = PUFRandom.getGaussian(MEAN_DELAY, STDDEV_DELAY);
			if (_sigma_0[i] < 0.0) _sigma_0[i] = -_sigma_0[i];
			_sigma_1[i] = PUFRandom.getGaussian(MEAN_DELAY, STDDEV_DELAY);
			if (_sigma_1[i] < 0.0) _sigma_1[i] = -_sigma_1[i];
		}
	}
	
	private boolean[] generatePhi(Challenge challenge)
	{
		boolean[] phi = new boolean[challenge.getSize()];
		
		for (int i = 0; i < getChallengeSize(); i++) {
			if (i == 0) {
				phi[i] = false;
			} else {
				phi[i] = phi[i - 1];
			}
			
			try {
				if (challenge.getBit(i)) {
					phi[i] = !phi[i];
				}
			} catch (InvalidBitIndexException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		return phi;
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		boolean[] phi = generatePhi(challenge);
		
		double delay_0 = 0.0, delay_1 = 0.0;
		
		for (int i = 0; i < this.getChallengeSize(); i++) {
			if (phi[i]) {
				delay_0 += _sigma_1[i];
				delay_1 += _sigma_0[i];
			} else {
				delay_0 += _sigma_0[i];
				delay_1 += _sigma_1[i];
			}
		}
		
		Response response = this.getCorrectlySizedResponse();
		
		if ((delay_1 - delay_0) > 0.0) {
			try {
				response.setValue(BigInteger.ONE);
			} catch (InvalidValueException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		if (addNoise) response.addNoise(PROBABILITY_OF_BIT_FLIP);
		
		return response;
	}

	public String doubleArrayToString(double[] array)
	{
		String output = "";
		
		for (int i = 0; i < array.length; i++) {
			output += i + ":" + array[i] + " ";
		}
		
		return output;
	}

	@Override
	protected void onRelease()
	{
		// Do nothing
	}

}
