package puf;

import puf.ArbiterKXORPUF.InvalidXORSizeException;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;

public class ArbiterXORPUFFactory extends PUFFactory
{
	private int _challengeSize;
	private int _responseSize;
	private int _xorSize;
	
	public ArbiterXORPUFFactory(int challengeSize, int responseSize, int xorSize) throws InvalidChallengeSizeException, InvalidResponseSizeException, InvalidXORSizeException
	{
		PUF.InvalidChallengeSizeException.testSettingChallengeSize(challengeSize);
		PUF.InvalidResponseSizeException.testSettingResponseSize(responseSize);
		ArbiterKXORPUF.InvalidXORSizeException.testSettingXORSize(xorSize);
		
		_challengeSize = challengeSize;
		_responseSize = responseSize;
		_xorSize = xorSize;
	}
	
	@Override
	protected PUF manufacturePUF()
	{
		try {
			return new ArbiterKXORPUF(_challengeSize, _responseSize, _xorSize);
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException | InvalidXORSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}

	@Override
	protected Server provisionServer()
	{
		try {
			return new ArbiterXORPUFServer(_xorSize);
		} catch (InvalidXORSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
}
