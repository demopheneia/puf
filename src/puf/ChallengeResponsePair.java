package puf;

import puf.PUF.Challenge;
import puf.PUF.Response;

public class ChallengeResponsePair
{
	private Challenge _challenge;
	private Response _response;
	
	public static ChallengeResponsePair create(Challenge challenge, Response response)
	{
		if (challenge == null || response == null) return null;
		
		return new ChallengeResponsePair(challenge, response);
	}
	
	private ChallengeResponsePair(Challenge challenge, Response response)
	{
		_challenge = challenge;
		_response = response;
	}
	
	public Challenge getChallenge()
	{
		return _challenge.clone();
	}
	
	public Response getResponse()
	{
		return _response.clone();
	}
	
	public String toString(boolean pretty)
	{
		if (pretty) {
			return "Challenge: " + _challenge.toString() + ", Response: " + _response.toString();
		} else {
			return _challenge.toString() + " " + _response.toString();
		}
	}
	
	@Override
	public String toString()
	{
		return this.toString(true);
	}
}
