package puf;

import java.util.HashMap;

public abstract class PUFFactory
{
	HashMap<PUF, Server> _servers;
	
	public PUFFactory()
	{
		_servers = new HashMap<PUF, Server>();
	}
	
	public Pair<PUF, Server> purchase()
	{
		PUF puf = manufacturePUF();
		
		Server server = provisionServer();
		
		server.enrol(puf);
		
		puf.release();
		
		_servers.put(puf, server);
		
		return new Pair<PUF, Server>(puf, server);
	}
	
	protected abstract PUF manufacturePUF();
	
	protected abstract Server provisionServer();
}
