package puf;

import java.util.HashMap;
import java.util.Set;

import puf.PUF.Challenge;
import puf.PUF.Response;

public final class Enrolment extends HashMap<Challenge, ChallengeResponsePair>
{
	/** Generated UID. */
	private static final long serialVersionUID = -1505576210404614793L;
	
	private Challenge[] _challenges = null;
	
	public Enrolment()
	{
		super();
	}
	
	public Enrolment(int initialCapacity)
	{
		super(initialCapacity);
	}
	
	public Set<Challenge> getChallenges()
	{
		return keySet();
	}
	
	public Challenge getRandomChallenge()
	{
		if (size() == 0) return null;
		
		if (_challenges == null) _challenges = getChallenges().toArray(new Challenge[1]);
		
		int selected = PUFRandom.getInt(0, _challenges.length);
		
		return _challenges[selected];
	}
	
	public Response getResponse(Challenge challenge)
	{
		if (challenge == null) return null;
		
		ChallengeResponsePair crp = get(challenge);
		
		if (crp == null) return null;
		
		return crp.getResponse();
	}
}
