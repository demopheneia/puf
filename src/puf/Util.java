package puf;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Util
{
	public static String percent(double n, double d, int decPlaces)
	{
		double digits = (n / d) * 100;
		
		return String.format("%." + decPlaces + "f", digits) + "%";
	}
	
	public static <T> Set<T> SetFromArray(T[] array)
	{
		if (array == null) return null;
		
		Set<T> set = new HashSet<T>();
		
		for (T item : array) {
			set.add(item);
		}
		
		return set;
	}

	private static ArrayList<BigInteger> _factorialMemo = new ArrayList<BigInteger>(128);
	
	public static BigInteger factorial(int n)
	{
		if (n < 0) {
			return null;
		} else if (n == 0 || n == 1) {
			return BigInteger.ONE;
		} else {
			if (_factorialMemo.size() < (n - 1)) {
				BigInteger prior = factorial(n - 1);
				prior = prior.multiply(BigInteger.valueOf(n));
				_factorialMemo.add(prior);
				return prior;
			} else {
				return _factorialMemo.get(n - 2);
			}
		}
	}
	
	public static BigInteger choose(int n, int k)
	{
		if (n < 0 || k < 0) return null;
		
		if (n < k) return BigInteger.ZERO;
		
		return factorial(n).divide(factorial(k).multiply(factorial(n - k)));
	}
	
	public static int[] chooseEnumerate(int n, int k)
	{
		int chooseNK = choose(n, k).intValueExact();
		
		if (chooseNK == 0) return null;
		
		int[] ret = new int[chooseNK * k];
		int[] template = new int[k];
		int nextInsertIndex = 0;
		
		// Initialise the template
		for (int i = 0; i < k; i++) {
			template[i] = i;
		}
		
		while (template[0] != -1) {
			for (int i = 0; i < k; i++) {
				ret[nextInsertIndex + i] = template[i];
			}
			nextInsertIndex += k;
			chooseEnumerateNextTemplate(n, template);
		}
		
		return ret;
	}
	
	public static void chooseEnumerateNextTemplate(int n, int[] template)
	{
		for (int i = template.length - 1; i >= 0; i--) {
			if (template[i] != n - template.length + i) {
				template[i]++;
				for (int j = 1; i + j < template.length; j++) template[i + j] = template[i] + j;
				return;
			}
		}
		
		// Completion sentinel value
		template[0] = -1;
		return;
	}
}
