package puf;

import java.util.Set;

import puf.FixedSizeInteger.InvalidSizeException;
import puf.FixedSizeInteger.InvalidValueException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public class EntanglementPUFServer extends Server
{
	private Enrolment _outputEnrolment = null;
	
	@Override
	public void enrol(PUF puf)
	{
		// Can only enrol once
		if (_enrolment != null) return;
		
		// Create the enrolment challenge set
		Set<Challenge> challenges = createEnrolmentChallenges(puf);
		
		// Initialise the enrolment
		_enrolment = new Enrolment(challenges.size() / 2);
		_outputEnrolment = new Enrolment(challenges.size() / 2);
		
		boolean onInput = false;
		
		// Create the required number of CRPs
		for (Challenge challenge : challenges) {
			
			onInput = !onInput;
			
			// Enrol the CRP
			ChallengeResponsePair crp;
			try {
				crp = puf.challenge(challenge, false);
				if (onInput) {
					_enrolment.put(challenge, crp);
				} else {
					_outputEnrolment.put(challenge, crp);
				}
			} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
				Simulation.shouldNeverHappen(e);
			}
		}
		
		// Calculate the allowed Hamming error for verification
		setMaxAllowableHammingError(puf);
	}
	
	@Override
	public Set<Challenge> createEnrolmentChallenges(PUF puf)
	{
		return ArbiterPUFServer.createEnrolmentChallengesPublic(puf, 2 * ArbiterPUFServer.NUMBER_OF_CHALLENGES_TO_ENROL);
	}

	@Override
	public boolean verify(PUF puf, Attacker watcher)
	{
		try {
			Challenge inputChallenge = getRandomEnrolledChallenge();
			Response inputResponse = getEnrolledResponse(inputChallenge);
			Challenge outputChallenge = getRandomEnrolledChallenge(_outputEnrolment);
			Response outputResponse = getEnrolledResponse(outputChallenge, _outputEnrolment);
			
			FixedSizeInteger randomVerificationValue = FixedSizeInteger.newRandom(puf.getResponseSize());
			FixedSizeInteger xor = randomVerificationValue.clone();
			xor.xor(inputResponse);
			
			Challenge concatenatedChallenge = new Challenge(inputChallenge.getSize() + outputChallenge.getSize() + inputResponse.getSize());
			concatenatedChallenge.setValue(FixedSizeInteger.concatenate(xor, outputChallenge, inputChallenge).getBits());
			
			Response pufResponse = puf.generateResponse(concatenatedChallenge, true);
			
			if (pufResponse == null) return false;
			
			// The attacker sees the challenge and response
			if (watcher != null) watcher.seeVerificationEvent(concatenatedChallenge, pufResponse.clone());
			
			pufResponse.xor(outputResponse);
			
			return pufResponse.hammingDistanceTo(randomVerificationValue) <= this.getMaxAllowableHammingError();
		} catch (InvalidSizeException | InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
			return false;
		}
	}

	@Override
	protected double calculateFalseNegativeProbability(int responseSize, int allowableFlippedBits)
	{	
		double probability = 0.0;
		double pBitFlipped = ArbiterPUF.PROBABILITY_OF_BIT_FLIP;
		double pBitUnflipped = 1.0 - pBitFlipped;
		
		double pXorFlipped = 2 * pBitFlipped * pBitUnflipped;
		double pXorUnflipped = 1.0 - pXorFlipped;
		
		for (int i = 0; i <= allowableFlippedBits; i++) {
			double branchCount = Util.choose(responseSize, i).doubleValue();
			double productPFlipped = Math.pow(pXorFlipped, i);
			double productPUnflipped = Math.pow(pXorUnflipped, responseSize - i);
			probability += branchCount * productPFlipped * productPUnflipped;
		}
		
		return 1.0 - probability;
	}

	@Override
	public boolean isExhausted()
	{
		// EntanglementPUFServer never runs out of enrolled CRPs
		return false;
	}
	
}
