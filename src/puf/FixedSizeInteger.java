package puf;

import java.math.BigInteger;
import java.util.Collection;

public class FixedSizeInteger
{
	public final BigInteger TWO_TO_THE_32ND_POWER = BigInteger.ONE.shiftLeft(32);
	
	private BigInteger _bits;
	private int _size;
	
	public FixedSizeInteger(int size) throws InvalidSizeException
	{
		InvalidSizeException.testAndThrow(size);
		
		_size = size;
		_bits = BigInteger.ZERO;
	}
	
	public FixedSizeInteger(FixedSizeInteger other) throws InvalidValueException
	{
		_size = other._size;
		
		InvalidValueException.testAndThrow(other == null ? null : other._bits, this);
		
		_bits = other._bits;
	}
	
	public void setValue(BigInteger value) throws InvalidValueException
	{
		InvalidValueException.testAndThrow(value, this);
		
		_bits = value;
	}
	
	public void random()
	{
		_bits = new BigInteger(_size, PUFRandom.getInstance());
	}
	
	public int getSize()
	{
		return _size;
	}
	
	public BigInteger getBits()
	{
		return _bits;
	}
	
	public String toString()
	{
		int nHexDigits = (_size - 1) / 4 + 1;
		return "0x" + String.format("%1$" + nHexDigits + "s", _bits.toString(16)).replace(' ', '0');
	}
	
	public boolean getBit(int index) throws InvalidBitIndexException
	{
		InvalidBitIndexException.testAndThrow(index, this);
		
		return _bits.testBit(index);
	}
	
	public FixedSizeInteger getBits(int lowerBitIndex, int upperBitIndex) throws InvalidBitIndexException
	{
		InvalidBitIndexException.testAndThrow(lowerBitIndex, this);
		InvalidBitIndexException.testAndThrow(upperBitIndex, this);
		
		if (lowerBitIndex > upperBitIndex) {
			int temp = lowerBitIndex;
			lowerBitIndex = upperBitIndex;
			upperBitIndex = temp;
		}
		
		try {
			int size = upperBitIndex - lowerBitIndex + 1;
			FixedSizeInteger ret = new FixedSizeInteger(size);
			ret.setValue(this.getBits().shiftRight(lowerBitIndex).mod(BigInteger.ONE.shiftLeft(size)));
			return ret;
		} catch (InvalidSizeException | InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	public void setBit(int index) throws InvalidBitIndexException
	{
		InvalidBitIndexException.testAndThrow(index, this);
		
		_bits = _bits.setBit(index);
	}
	
	public void xorBit(int index, boolean bit) throws InvalidBitIndexException
	{
		InvalidBitIndexException.testAndThrow(index, this);
		
		if (_bits.testBit(index) ^ bit) {
			_bits = _bits.setBit(index);
		} else {
			_bits = _bits.clearBit(index);
		}
	}
	
	public void xor(FixedSizeInteger other) throws InvalidSizeException
	{
		InvalidSizeException.testAndThrow(other, this);
		
		_bits = _bits.xor(other._bits);
	}
	
	/** Flips some of the bits in the integer with a given probability. */ 
	public void addNoise(double probabilityOfBitFlip)
	{
		try {
			// Create a zero integer of the same size as this one
			FixedSizeInteger noise = new FixedSizeInteger(this._size);
		
			// Flip some bits to create a noise vector
			for (int i = 0; i < noise.getSize(); i++) {
				if (probabilityOfBitFlip > PUFRandom.getDouble(0.0, 1.0)) noise.setBit(i);
			}
		
			// Apply the noise vector to this
			this.xor(noise);
			
		} catch (InvalidSizeException | InvalidBitIndexException e) {
			Simulation.shouldNeverHappen(e);
		}
	}
	
	public int hammingDistanceTo(FixedSizeInteger other) throws InvalidSizeException
	{
		FixedSizeInteger xor = this.clone();
		
		xor.xor(other);
		
		return xor.hammingLength();
	}
	
	public int hammingLength()
	{
		return _bits.bitCount();
	}
	
	public void fromWEKAValueArray(double[] valueArray) throws InvalidValueException
	{
		if (valueArray == null) return;
		
		if (valueArray.length != _size) return;
		
		BigInteger newValue = BigInteger.ZERO;

		for (int i = 0; i < _size; i++) {
			if (valueArray[i] == 1.0) newValue = newValue.setBit(i);
		}
		
		InvalidValueException.testAndThrow(newValue, this);
		
		_bits = newValue;
	}
	
	@Override
	public boolean equals(Object other)
	{
		return (other instanceof FixedSizeInteger && equals((FixedSizeInteger) other));
	}
	
	public boolean equals(FixedSizeInteger other)
	{
		return (_size == other._size) && (_bits.equals(other._bits));
	}
	
	@Override
	public int hashCode()
	{
		return _bits.hashCode();
	}

	public static FixedSizeInteger noiseReducedValue(Collection<? extends FixedSizeInteger> samples)
	{
		if (samples == null || samples.isEmpty()) return null;
		
		try {
			int nBits = samples.iterator().next().getSize();
			int[] accum = new int[nBits];
			
			for (FixedSizeInteger sample : samples) {
				for (int i = 0; i < nBits; i++) {
					accum[i] += sample.getBit(i) == true ? 1 : -1;
				}
			}
			
			FixedSizeInteger ret = new FixedSizeInteger(nBits);
			for (int i = 0; i < nBits; i++) {
				if (accum[i] > 0) ret.setBit(i);
			}
			
			return ret;
		} catch (InvalidSizeException | InvalidBitIndexException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	public static FixedSizeInteger concatenate(FixedSizeInteger ... list)
	{
		if (list.length == 0) return null;
		
		int size = 0;
		BigInteger temp = BigInteger.ZERO;
		for (FixedSizeInteger x : list) {
			size += x.getSize();
			temp = temp.shiftLeft(x.getSize());
			temp = temp.or(x.getBits());
		}
		
		FixedSizeInteger ret = null;
		try {
			ret = new FixedSizeInteger(size);
			ret.setValue(temp);
		} catch (InvalidSizeException | InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
		}
		
		return ret;
	}
	
	public byte[] bytes()
	{
		int nBytes = (this.getSize() - 1) / 8 + 1;
		byte[] ret = new byte[nBytes];
		final BigInteger twoFiftySix = BigInteger.valueOf(256);
		final BigInteger oneTwentyEight = BigInteger.valueOf(128);
		for (int i = 0; i < nBytes; i++) {
			ret[i] = this.getBits().shiftRight(i * 8).mod(twoFiftySix).subtract(oneTwentyEight).byteValueExact();
		}
		return ret;
	}
	
	public void fromBytes(byte[] bytes)
	{
		if (bytes == null) return;

		int nBytes = (this.getSize() - 1) / 8 + 1;
		this._bits = BigInteger.ZERO;
		for (int i = nBytes < bytes.length ? nBytes - 1 : bytes.length - 1; i >= 0; i--) {
			this._bits = this._bits.shiftLeft(8).or(BigInteger.valueOf(bytes[i]));
		}
	}
	
	@Override
	public FixedSizeInteger clone()
	{
		try {
			return new FixedSizeInteger(this);
		} catch (InvalidValueException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	/**
	 * Creates a new FixedSizeInteger of the given size, initialised with a random value.
	 * 
	 * @param size
	 * 			The size of the new FixedSizeInteger in bits.
	 * 
	 * @return
	 * 			A random FixedSizeInteger of the given size.
	 * 
	 * @throws InvalidSizeException
	 */
	public static FixedSizeInteger newRandom(int size) throws InvalidSizeException
	{
		FixedSizeInteger ret = new FixedSizeInteger(size);
		
		ret.random();
		
		return ret;
	}
	
	/** Exception to throw when trying to store an invalid value in a FixedSizeInteger. */
	public static class InvalidValueException extends Exception
	{
		private static final long serialVersionUID = 5654191895591256301L;
		
		/**
		 * Tests if the given value can be stored in the given destination, and
		 * throws an InvalidValueException if not.
		 */
		public static void testAndThrow(BigInteger value, FixedSizeInteger dest) throws InvalidValueException
		{
			String message = null;
			
			if (dest == null) {
				message = "Cannot store value in null FixedSizeInteger";
			} else if (value == null) {
				message = "Cannot store null value in FixedSizeInteger";
			} else if (value.signum() == -1) {
				message = "Cannot store negative value in FixedSizeInteger";
			} else if (value.bitLength() > dest._size) {
				message = "Cannot store " + value.toString(16) + " (" + value.bitLength() + "-bit) in " + dest._size + "-bit FixedSizeInteger";
			}
			
			if (message != null) {
				throw new InvalidValueException(message);
			}
		}
		
		private InvalidValueException(String message)
		{
			super(message);
		}
	}
	
	/** Exception to throw when trying to make a FixedSizeInteger with a size <= 0. */
	public static class InvalidSizeException extends Exception
	{
		private static final long serialVersionUID = 7553695662034114467L;
		
		public static void testAndThrow(int size) throws InvalidSizeException
		{
			if (size <= 0) throw new InvalidSizeException("FixedSizeInteger must have size greater than zero.");
		}
		
		public static void testAndThrow(FixedSizeInteger source, FixedSizeInteger dest) throws InvalidSizeException
		{
			if (source._size != dest._size) throw new InvalidSizeException("Source size (" + source._size + ") must match destination size (" + dest._size + ")");
		}
		
		private InvalidSizeException(String message)
		{
			super(message);
		}
	}
	
	/** Exception to throw when trying to access a bit in the FixedSizeInteger that is out of range. */
	public static class InvalidBitIndexException extends Exception
	{
		private static final long serialVersionUID = -1203818017859092021L;
		
		public static void testAndThrow(int bitIndex, FixedSizeInteger target) throws InvalidBitIndexException
		{
			if (!(0 <= bitIndex && bitIndex < target._size)) throw new InvalidBitIndexException("Bit-index " + bitIndex + " is out of range (0 - " + (target._size - 1) + ")");
		}
		
		private InvalidBitIndexException(String message)
		{
			super(message);
		}
	}
}
