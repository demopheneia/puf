package puf;

import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;

public class EntanglementPUFFactory extends PUFFactory
{
	private int _challengeSize;
	private int _responseSize;
	
	public EntanglementPUFFactory(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		PUF.InvalidChallengeSizeException.testSettingChallengeSize(challengeSize);
		PUF.InvalidResponseSizeException.testSettingResponseSize(responseSize);
		
		_challengeSize = challengeSize;
		_responseSize = responseSize;
	}
	
	@Override
	protected PUF manufacturePUF()
	{
		try {
			return new EntanglementPUF(_challengeSize, _responseSize);
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}

	@Override
	protected Server provisionServer()
	{
		return new EntanglementPUFServer();
	}

}
