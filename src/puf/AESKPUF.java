package puf;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import puf.FixedSizeInteger.InvalidSizeException;

public class AESKPUF extends ArbiterKPUF
{
	public static final int AES_KEY_SIZE = 128;
	private byte[] _AESKey;
	
	protected AESKPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize);
		
		try {
			FixedSizeInteger key = new FixedSizeInteger(AES_KEY_SIZE);
			key.random();
			_AESKey = key.bytes();
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
		}
	}

	@Override
	protected Response generateResponse(Challenge challenge, boolean addNoise)
	{
		// Encrypt challenge with AES
		SecretKeySpec secretKey = new SecretKeySpec(_AESKey, "AES");
		byte[] cipherText;
		try {
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	        cipherText = cipher.doFinal(challenge.bytes());
		} catch (Exception e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
		
		Challenge encryptedChallenge = null;
		try {
			encryptedChallenge = new Challenge(challenge.getSize());
			encryptedChallenge.fromBytes(cipherText);
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
		
		return super.generateResponse(encryptedChallenge, addNoise);
	}
}
