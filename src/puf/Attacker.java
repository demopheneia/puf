package puf;

import puf.FixedSizeInteger.InvalidSizeException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;
import puf.PUF.Response;

public abstract class Attacker
{
	private DummyPUF _dummyPUF = null;
	private Server _attackedServer = null;
	
	public Attacker(int challengeSize, int responseSize, Server server) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		instantiateDummyPUF(challengeSize, responseSize);
		learnAboutServer(server);
	}
	
	public abstract void seeVerificationEvent(Challenge challenge, Response response);
	
	public abstract void getDirectAccessToPUF(PUF puf);
	
	public abstract Response guessResponse(Challenge challenge);
	
	public abstract int getExpandedChallengeSize();
	
	public int estimateDesiredNumberOfCRPs()
	{
		// Calculate the number of CRPs to enrol
		// N = 0.5 * (k + 1) / e * safety_factor
		// safety_factor == 2
		return (int) ((getExpandedChallengeSize() + 1) / (getAttackedServer().getAchievedFalseNegativeProbability()));
	}
	
	protected final void instantiateDummyPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		if (_dummyPUF == null) _dummyPUF = new DummyPUF(challengeSize, responseSize);
	}
	
	protected final void discardDummyPUF()
	{
		_dummyPUF = null;
	}
	
	public final PUF getDummyPUF()
	{
		return _dummyPUF;
	}
	
	public final void learnAboutServer(Server server)
	{
		_attackedServer = server;
	}
	
	protected final Server getAttackedServer()
	{
		return _attackedServer;
	}
	
	protected final Challenge createExpandedChallenge()
	{
		try {
			int expandedChallengeSize = getExpandedChallengeSize();
			Challenge expandedChallenge = new Challenge(expandedChallengeSize);
			return expandedChallenge;
		} catch (InvalidSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
	
	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " :\n" +
				"  #xC = " + getExpandedChallengeSize() + "\n" +
				"  #dCRPs = " + estimateDesiredNumberOfCRPs();
	}
	
	protected class DummyPUF extends PUF
	{
		protected DummyPUF(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
		{
			super(challengeSize, responseSize);
		}

		@Override
		protected Response generateResponse(Challenge challenge, boolean addNoise)
		{
			return Attacker.this.guessResponse(challenge);
		}

		@Override
		protected void onRelease()
		{
			// Do nothing
		}
		
	}
}
