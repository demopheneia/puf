package puf;

import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;

public class AESKPUFFactory extends ArbiterPUFFactory
{
	public AESKPUFFactory(int challengeSize, int responseSize) throws InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize);
	}

	@Override
	protected PUF manufacturePUF()
	{
		try {
			return new AESKPUF(_challengeSize, _responseSize);
		} catch (InvalidChallengeSizeException | InvalidResponseSizeException e) {
			Simulation.shouldNeverHappen(e);
			return null;
		}
	}
}
