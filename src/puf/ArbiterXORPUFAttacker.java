package puf;

import puf.ArbiterKXORPUF.InvalidXORSizeException;
import puf.FixedSizeInteger.InvalidBitIndexException;
import puf.PUF.Challenge;
import puf.PUF.InvalidChallengeSizeException;
import puf.PUF.InvalidResponseSizeException;

public class ArbiterXORPUFAttacker extends ArbiterPUFAttacker
{
	private int _xorSize;
	private int _expandedChallengeSize = 0;
	
	public ArbiterXORPUFAttacker(int challengeSize, int responseSize, Server server, int xorSize) throws InvalidXORSizeException, InvalidChallengeSizeException, InvalidResponseSizeException
	{
		super(challengeSize, responseSize, server);
		
		ArbiterKXORPUF.InvalidXORSizeException.testSettingXORSize(xorSize);
		
		_xorSize = xorSize;
	}
	
	@Override
	protected Challenge formatChallenge(Challenge challenge)
	{
		return crossXORChallenge(phiIfyChallenge(challenge));
	}
	
	protected Challenge crossXORChallenge(Challenge challenge)
	{
		if (challenge == null) return null;
		
		Challenge ret = createExpandedChallenge();
		
		int retIndex = 0;
		for (int xorIndex = _xorSize; xorIndex >= 1; xorIndex -= 2) {
			int[] xorEnumerations = Util.chooseEnumerate(challenge.getSize(), xorIndex);
			
			for (int enumerationIndex = 0; enumerationIndex < xorEnumerations.length; enumerationIndex += xorIndex) {
				for (int bitIndex = 0; bitIndex < xorIndex; bitIndex++) {
					try {
						ret.xorBit(retIndex, challenge.getBit(xorEnumerations[enumerationIndex + bitIndex]));
					} catch (InvalidBitIndexException e) {
						Simulation.shouldNeverHappen(e);
					}
				}
				retIndex++;
			}
		}
		
		return ret;
	}
	
	@Override
	public int getExpandedChallengeSize()
	{
		if (_expandedChallengeSize != 0) return _expandedChallengeSize;
		_expandedChallengeSize = 0;
		for (int i = _xorSize; i >= 1; i -= 2) {
			_expandedChallengeSize += Util.choose(this.getDummyPUF().getChallengeSize(), i).intValueExact();
		}
		return _expandedChallengeSize;
	}
}
